const http = require("http");
const port = 4000;

let mockData = [
    {
        "firstName": "Mary Jane",
        "lastName": "Dela Cruz",
        "mobileNo": "09123456789",
        "email": "mjdelacruz@mail.com",
        "password": 123
        },
        {
        "firstName": "John",
        "lastName": "Doe",
        "mobileNo": "09123456789",
        "email": "jdoe@mail.com",
        "password": 123
        }
        
]

const server = http.createServer((request, response) => {

    if(request.url == "/profile" && request.method == "GET") {
        response.writeHead(200, {"Content-Type":"application/json"})
        response.write(JSON.stringify(mockData))
        response.end()

    }

    if(request.url == "/profile" && request.method == "POST") {
        
        let requestBody = ""
        
        request.on('data', function(data){
            requestBody += data;
        })
        
        request.on('end', function(){

            console.log(typeof requestBody)
            requestBody = JSON.parse(requestBody)

            let newProfile = {
                "firstName": requestBody.firstName,
                "lastName": requestBody.lastName,
                "mobileNo": requestBody.mobileNo,
                "email": requestBody.email,
                "password": requestBody.password
            }
            
            mockData.push(newProfile);
            console.log(mockData)

            response.writeHead(200, {"Content-Type":"application/json"})
            response.write(JSON.stringify(newProfile))
            response.end()
        })
    }

    //stretch goals
    //tests only
    //DELETE

    if(request.url == "/profile" && request.method == "DELETE") {
        
        //input value of first name to delete

        var removeByAttr = function(arr, attr, value){
            var i = arr.length;
            while(i--){
               if( arr[i] 
                   && arr[i].hasOwnProperty(attr) 
                   && (arguments.length > 2 && arr[i][attr] === value ) ){ 
        
                   arr.splice(i,1);
        
               }
            }
            return arr;
        }
        
        let requestBody = ""
        
        request.on('data', function(data){
            requestBody += data;
        })
        
        request.on('end', function(){

            console.log(typeof requestBody)
            let findData = ""

            if(requestBody == "" || requestBody == null) {
                response.end()
            } else {
                findData = mockData.find(x => x.firstName === requestBody)
                if(findData == "" || findData == null) {
                    response.write("No data found to delete")
                    response.end()
                } else {

                    removeByAttr(mockData, 'firstName', requestBody);
                    response.writeHead(200, {"Content-Type":"text/plain"})
                    response.write(JSON.stringify(requestBody + " has been removed"))
                    console.log(mockData)
                    response.end()
                }    
            }    
        })
    }

    //stretch goals
    //tests only
    //PUT

    if(request.url == "/profile" && request.method == "PUT") {
        
        //input value of first name to update
        //then 2nd input - value of the update

        let requestBody = ""
        
        request.on('data', function(data){
            requestBody += data;
        })

        
        request.on('end', function(){

            console.log(typeof requestBody)
            requestBody = JSON.parse(requestBody)

            if(requestBody.firstName == "" || requestBody.firstName == null) {
                response.end()
            } else {
                findData = mockData.find(x => x.firstName === requestBody.firstName)
                if(findData == "" || findData == null) {
                    response.write("No data found to update")
                    response.end()
                } else {

                    let objIndex = mockData.findIndex((obj => obj.firstName === requestBody.firstName));
                    mockData[objIndex].firstName = requestBody.update

                    response.writeHead(200, {"Content-Type":"text/plain"})
                    response.write(JSON.stringify(requestBody))
                    console.log(mockData)
                    response.end()
                }    
            }  

        })
    }

})

server.listen(port);

console.log("Server is successfully running")