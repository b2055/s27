const http = require("http");
const port = 4000;

const server = http.createServer((request, response) => {

    //The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
    //The method "GET" means that we will be retreiving or reading information
    if(request.url == "/items" && request.method == "GET") {
        response.writeHead(200, {"Content-Type":"text/plain"})
        response.end("Data retreived from the database")
    }

    if(request.url == "/items" && request.method == "POST") {
        response.writeHead(200, {"Content-Type":"text/plain"})
        response.end("Data to be sent to the database")
    }

    if(request.url == "/items" && request.method == "PUT") {
        response.writeHead(200, {"Content-Type":"text/plain"})
        response.end("Update our resources")
    }

    if(request.url == "/items" && request.method == "DELETE") {
        response.writeHead(200, {"Content-Type":"text/plain"})
        response.end("Delete our resources")
    }

})

server.listen(port);

console.log("Server is successfully running")